package Lab10;


public class Robot extends Thread{
	static int[][] space2D = new int[101][101];
	boolean crashed=false;
	Position p;
	
public static void main(String[] args) {
	
    Position p = new Position();
    Robot r1 = new Robot("r1",p);
    Robot r2 = new Robot("r2",p);
    Robot r3 = new Robot("r3",p);
    Robot r4 = new Robot("r4",p);
    Robot r5= new Robot("r5",p);
    Robot r6 = new Robot("r6",p);
    Robot r7 = new Robot("r7",p);
    Robot r8 = new Robot("r8",p);
    Robot r9 = new Robot("r9",p);
    Robot r10 = new Robot("r10",p);
 
    r1.start();
    r2.start();
    r3.start();
    r4.start();
    r5.start();
    r6.start();
    r7.start();
    r8.start();
    r9.start();
    r10.start();

  }


	
public Robot(String name,Position p){
	super(name);
    this.p = p;
   // space2D[this.p.getX()][this.p.getY()]++;
}
	public void run(){
        int i =0;
        while(++i<30){ //robots move 30 times
        	 if(crashed==false) {
                 if (space2D[p.getX()][p.getY()] > 1) {
                     interrupt(); 
                 } 
                 else 
                 {
                	 --space2D[p.getX()][p.getY()]; 
                	 int a = (int)Math.round(100*Math.random());
                     int b = (int)Math.round(100*Math.random());
                          
                     synchronized(p){ 
                     p.setXY(a,b);}
                     space2D[p.getX()][p.getY()]++;
                     System.out.println("Robot "+getName()+" moved to position: ["+p.getX()+","+p.getY()+"]");
                     
                  }
            try {
                sleep(100);
            } catch (InterruptedException e) {
 
                e.printStackTrace();
            }
               
        } } if(!crashed)
                System.out.println(getName() + " has finished moving"); } //message at end of a robot moving
        
        public void interrupt(){
            System.out.println(getName()+" at position: ["+p.getX()+","+p.getY()+"]"+ " has crashed");
            crashed=true;
        }
}

class Position {
    int x,y;
    public void setXY(int a,int b){
        x = a;y = b;
    }  
    public int getX(){return x;}
    public int getY(){return y;}  
}

