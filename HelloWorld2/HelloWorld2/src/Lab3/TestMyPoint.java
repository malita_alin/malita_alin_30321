package Lab3;

public class TestMyPoint {

	public static void main(String[] args){
		MyPoint point1= new MyPoint();
		point1.setX(1);
		point1.setY(3);
		System.out.println("X of point1: "+point1.getX()+" and Y of point1: "+point1.getY()) ;
		MyPoint point2 =new MyPoint(5,5);
		System.out.println(point2);
		System.out.println("Distance between point1 and (6,6) =  "+point1.distance(6,6));
		System.out.println("Distance between point2 and point1 =  "+point2.distance(point1));
		
	}
}
