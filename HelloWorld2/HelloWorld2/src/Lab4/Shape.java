package Lab4;

public class Shape {
 private  String color="red";
  private boolean filled = true;
   
 public Shape() {this.color="green"; this.filled = true; }
 public Shape(String color, boolean filled) {this.color=color; this.filled=filled; }
  
 public  String getColor() {return this.color;}
 public void setColor(String color) {this.color=color;}
 public  boolean isFILLED() {return this.filled;}
 public  void setFILLED(boolean filled) {this.filled=filled;}
  
  public String toString() 
  {   
	  if(this.filled)
  {return "A Shape with color of "+ this.color +" and "+ "filled";}
  else{
	  return "A Shape with color of "+ this.color +" and "+ "Not filled";}
} }



class Circle1 extends Shape{
	double radius=1.0;
	
	public Circle1(){
		super();
		this.radius=1.0;}
	
	public Circle1(double radius){
		super();
		this.radius=radius;
		}
	public Circle1(double radius, String color, boolean filled){
		super(color, filled);
		this.radius=radius;	}
	
	public double getRadius() {return radius; }
	public void setRadius(double radius) {this.radius=radius;}
	public double getPerimeter() {return 2*radius * Math.PI; }
	public double getArea() {return Math.pow(radius,2) * Math.PI; }

 	public String toString()
 	{return "A Circle with radius="+this.radius+", which is a subclass of "+super.toString(); }
}



class Rectangle extends Shape{
  private double width = 1.0;
  private double length = 1.0;
  
 public Rectangle(){
	 super();
	 this.width=1.0;
	 this.length=1.0;
 }
 
 public Rectangle(double witdh, double length){
	 super();
	 this.width=witdh;
	 this.length=length;
 }
 
 public Rectangle(double witdh, double length,String color, boolean filled){
	 super(color, filled);
	 this.width=witdh;
	 this.length=length;
 }
  
 public double getWidth() {return this.width; }
 public void setWidth(double width) {this.width=width;}
 public double getLength() {return this.length; }
 public void setLength(double length) {this.length=length;}
	
 public double getPerimeter() {return 2*(this.width+this.length); }
 public double getArea() {return this.width*this.length; }
	
 	public String toString()
 	{return "A Rectangle with width=" +this.width+ " and length="+ this.length+", which is a subclass of "+super.toString();}
}


class Square extends Rectangle{
   public Square() {
	   super();
   }
   
   public Square(double side) {
	   super(side,side);
   }
   public Square(double side, String color, boolean filled) {
	   super(side,side,color, filled);
   }
   
   
   public double getSide() {return this.getWidth(); }
   public 	void setSide(double side) {super.setWidth(side);
 	super.setLength(side);}
 	
   public 	void setWidth(double side) {super.setWidth(side);
	super.setLength(side);}
   public 	void setLength(double side) {super.setWidth(side);
	super.setLength(side);}
  	
  	public String toString() 
  	{return "A Square with side="+this.getSide()+", which is a subclass of "+super.toString();}
}


  class ShapeTest{
		public static void main(String[] args){
			//ShapeTest
			Shape shape1= new Shape();
			Shape shape2= new Shape("blue",false);
			System.out.println("\nShape2 of color "+shape2.getColor()+" and is filled="+shape2.isFILLED());
			shape1.setColor("yellow"); shape1.setFILLED(false);
			System.out.println(shape1);
			
			//Circle1Test
			Circle1 circle1= new Circle1();
			Circle1 circle2= new Circle1(6);
			Circle1 circle3= new Circle1(5, "blue", true);
			System.out.println("\nCircle2 of radius="+circle2.getRadius()+", area = "+circle2.getArea()+", perimeter="+circle2.getPerimeter()+", color "+circle2.getColor()+" and is filled="+circle2.isFILLED());
			circle1.setColor("yellow"); 	circle1.setFILLED(false); circle1.setRadius(9);
			System.out.println(circle1);

			//RectangleTest
			Rectangle rectangle1=new Rectangle();
			Rectangle rectangle2=new Rectangle(5,6);
			Rectangle rectangle3=new Rectangle(4,3,"orange",true);
			System.out.println("\nRectangle2 of width"+rectangle2.getWidth()+", length="+rectangle2.getLength()+", area = "+rectangle2.getArea()+", perimeter="+rectangle2.getPerimeter()+", color "+rectangle2.getColor()+" and is filled="+rectangle2.isFILLED());
			rectangle1.setColor("yellow"); 	rectangle1.setFILLED(false); rectangle1.setWidth(9); rectangle1.setLength(4);
			System.out.println(rectangle1);

			//SquareTest
			Square square1=new Square();
			Square square2=new Square(20,"black",false);
			Square square3=new Square(10);
			System.out.println("\nSquare2 of side="+square2.getSide()+", area = "+square2.getArea()+", perimeter="+square2.getPerimeter()+", color "+square2.getColor()+" and is filled="+square2.isFILLED());
			square1.setColor("yellow"); square1.setFILLED(false); square1.setSide(9);
			System.out.println(square1);
		}
  }
		
