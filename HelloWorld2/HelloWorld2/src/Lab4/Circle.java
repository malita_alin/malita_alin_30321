package Lab4;

public class Circle {

	private double radius=1.0;
	private String color="red";
	
	Circle(double radius){
		this.radius=radius;
		this.color="red";}
	
	Circle(){
		this.radius=1.0;
		this.color="red";}
	
	public double getRadius() {return radius; }
	public double getArea() {return Math.pow(radius,2) * Math.PI; }
}
	
  class CircleTest {
		public static void main(String[] args){
			Circle circle2 = new Circle();
			Circle circle1 = new Circle(5);
			System.out.println("Radius of circle1: "+circle1.getRadius());
		   System.out.println("Area of circle1: "+circle1.getArea());
	}
	}

