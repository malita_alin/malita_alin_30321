package Lab4;

public class Author {
    private String name;
    private String email;
    private char gender;
    
    public Author (String name, String email, char gender) {
   	 this.name= name;
   	 this.email=email;
   	 this.gender=gender;
    }
    
    public   String getName() {return name; }
    public  String getEmail() {return email; }
    public   char getGender() {return gender; }
    public   void setEmail(String email) {this.email=email;}
    
    public String toString(){
		return name +" ("+gender+")"+" at "+email;
	}
}

 class TestAuthor {
	public static void main(String[] args){
		Author author = new Author("Malita Alin","malitaalin@email.com",'m');
		System.out.println("Email: "+author.getEmail()+"\n Author: "+author.getName()+"\n Gender:"+author.getGender());
		author.setEmail("malitaalin@yahoo.com");
		System.out.println(author);
		
	}
}