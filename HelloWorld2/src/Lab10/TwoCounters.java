package Lab10;


public class TwoCounters {

public static void main(String[] args) throws InterruptedException {
	
	Counter1 c1 = new Counter1("counter1",null);
    Counter1 c2 = new Counter1("counter2",c1);

    c1.start();
    c1.join();
    c2.start();

}
}

 class Counter1 extends Thread {
	 Thread t;
     static int counter; 
    Counter1(String name,Thread t){
          super(name);
          this.t=t;
    }

    public void start(){
   
    	if(t!=null)
			try {
				t.join();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
          for(int i=0;i<=100;i++){
                System.out.println(getName() + " i = "+counter);
              counter++;
                try {
                      Thread.sleep((int)(Math.random() * 100));
                } catch (InterruptedException e) {
                      e.printStackTrace();
                }
          }
          counter--;
          System.out.println(getName() + " job finalised.");
         
    } 
    }


	
 
 