package Lab10;

import java.awt.*;        
import java.awt.event.*;
import javax.swing.*;


//
//  This version of Exercise 6 does not work- attempt using notify, synchronize, wait methods with Thread
//
public class Cronometer  extends JFrame implements Runnable   {
	
	public Thread timer;
	private JLabel lblCount;    
	   private JTextArea tfCount; 
	   private JButton btnCount;   
	   private JButton btnReset;   
	   private long count = 0; // Counter's value
	   
	   private Thread runner;
	   private long startTime;   // Start time of timer.
	   private boolean isRunning=false;

	   public Cronometer() {
		   timer=new Thread(this);
		   
	      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           setTitle("Chronometer"); 
          setLayout(null);
	      
	      lblCount = new JLabel("Time");  
	      lblCount.setBounds(5,15,80, 30);
	      add(lblCount);                   

	      tfCount = new JTextArea(" "+count + " s"); 
	      tfCount.setBounds(50,15,160, 30);
	      tfCount.setFont(new Font("Arial",Font.BOLD, 18));
	      tfCount.setEditable(false);       
	      add(tfCount);                     

	      btnCount = new JButton("Start");   
	      btnCount.setBounds(225,15,80, 30);
	      add(btnCount);   
	      
	      
	      btnReset = new JButton("Reset"); 
	      btnReset.setBounds(315,15,80, 30);
	      add(btnReset); 

	      BtnCount listener = new BtnCount();
	      btnCount.addActionListener(listener);
	      
	      BtnReset resetlistener = new BtnReset();
	      btnReset.addActionListener(resetlistener);
	      
	      setSize(450, 150);        
	      setVisible(true);  
	      
	   }

	   
	   private class BtnCount implements ActionListener  {
	      @Override
	      public void actionPerformed(ActionEvent evt) {
	    	isRunning=!isRunning;
	    	  if (isRunning==true) {
	    		   run();}
	          /*if(isRunning==0){
	        	  try {
					timestop();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	             
	    	 } */
	      } 
	}
	   
	   public void run() {
		  // timer=new Thread();
		  /* synchronized(timer) {if(isRunning==0)
			try {
				timer.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} */
		   
		   synchronized(timer) {
			  
		   btnCount.setText("Pause");
		 while(btnCount.getText().equals("Pause")) {
         tfCount.setText(" "+count + " s");
         count++;
		   waitDelay(1000);
	   } }
 }
	   
	   void timestop() throws InterruptedException {
		   isRunning=false;
		   btnCount.setText("Resume");
		   synchronized(timer) {if(isRunning=true) timer.notify();  }
       }
	   
	   
	   synchronized void waitDelay(int milliseconds) {
           // Pause for the specified number of milliseconds or  until the notify() method is called by some other thread.
        try {
           wait(milliseconds);
        }
        catch (InterruptedException e) {
        }
     }
	   
	   
	   private class BtnReset implements ActionListener {
	      
	      @Override
	      public void actionPerformed(ActionEvent evt) {
	    	  count=0;
	    	 Cronometer.this.tfCount.setText(" "+count + " s");
	    	  btnCount.setText("Start");
	    	  isRunning=false;
	    	/* timer=null;
	    	synchronized(this){  try {
				timer.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }*/
	    	}
	   }
	   
	   
	// The entry main() method
	   public static void main(String[] args) {
	     Cronometer app = new Cronometer();
	     int count=0;
	     app.btnCount.setText("Pause");
	    app.run();

	   
	}
	   }