package Lab10;

import java.awt.Font;

import javax.swing.*;

//
//This version of Exercise 6 does work- uses SwingWorker
//
public class Chronometer extends JFrame{
	private JLabel lblCount; 
	   private JButton btnCount;   
	   private JButton btnReset; 
    static JTextArea tfCount; 
     
    
    static int count;
    static boolean isRunning=false;

    private Chronometer(){
    	 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         setTitle("Chronometer"); 
        setLayout(null);
        init();
        setSize(450, 150);        
	   setVisible(true);  
    }
    public void init(){
        
        lblCount = new JLabel("Time: ");  
	      lblCount.setBounds(5,15,80, 30);
	      lblCount.setFont(new Font("Arial",Font.BOLD, 16));
	      add(lblCount);  
	      
	      tfCount = new JTextArea(" "+count + " s"); 
	      tfCount.setBounds(50,20,160, 30);
	      tfCount.setFont(new Font("Arial",Font.BOLD, 18));
	      tfCount.setEditable(false);       
	      add(tfCount);     
	      
	      
        btnCount=new JButton("Start");
        btnCount.setBounds(225,15,80, 30);
        btnCount.addActionListener(action->{
            new SwingWorker(){
                @Override
                protected Object doInBackground() throws Exception {
                    isRunning=!isRunning;
                    btnCount.setText("Resume"); //this makes it so when chronometer is stopped, it displays Resume
                    while(isRunning){
                    	 btnCount.setText("Pause");
                        count++;
                        if(count>59) {  //displays minutes only after the first 59 seconds pass. count value is not modified
                        	int minute=count/60;
                           int second=count-60*minute;
                        	tfCount.setText(String.valueOf(" "+minute+" m:"+second + " s"));
                        }else {
                        tfCount.setText(String.valueOf(" "+count + " s"));}
                        try {
                           Thread.sleep(1000);
                        	
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    return null;
                };
            }.execute();
        });
        add(btnCount);

        btnReset=new JButton("Reset");
        btnReset.setBounds(315,15,80, 30);
        btnReset.addActionListener(action->{
            count=0;
            isRunning=false;
            tfCount.setText(String.valueOf(" "+count + " s"));
            btnCount.setText("Start"); //this resets the count but also the label of the button
        });
        add(btnReset);

    }

    static public void main(String[] args) {

                Chronometer app=new Chronometer();

    }
}