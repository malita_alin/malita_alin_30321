package Lab4;

public class Cylinder extends Circle{
   private double height= 1.0;
   
   Cylinder(){
	   super();
	   this.height=1.0;
   }
   
   Cylinder(double radius){
	   super(radius);
	   this.height=1.0;
   }
   
   Cylinder(double radius, double height){
	   super(radius);
	   this.height=height;
	   }
   public  double getHeight() {return this.height;}
   public  double getVolume() {return super.getArea()*height;}
}

class CylinderTest {
		public static void main(String[] args){
			Cylinder cylinder2 = new Cylinder();
			Cylinder cylinder3 = new Cylinder(5);
			Cylinder cylinder1 = new Cylinder(5,4);
			System.out.println("Height of cylinder1: "+cylinder1.getHeight());
		   System.out.println("Volume of cylinder1: "+cylinder1.getVolume());
	}
	}