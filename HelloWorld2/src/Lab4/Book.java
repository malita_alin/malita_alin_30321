package Lab4;

//comments for showing Exercise 3 code 
public class Book {
   private Author[] authors; //private Author author;
   private String name;
  private double price; 
   private int qtyInStock=0;
   
  public Book(String name, Author[] authors, double price){  //public Book(String name, Author author, double price){ 
	   this.name=name;
	   this.price=price;
	   this.authors=authors; //this.author=author;
   }
  public  Book(String name, Author[] authors, double price, int qtInStock){ //public Book(String name, Author author, double price, int qtInStock){ 
	   this.name=name;
	   this.price=price;
	   this.authors=authors;  //this.author=author;
	   this.qtyInStock=qtyInStock;
   }
   
  public  String getName() {return this.name;}
  public   void printAuthors() {
	   for(int i=0; i<authors.length; i++)
	   {
		   System.out.println("Author :"+authors[i]);
	   }   
   } //only for Exercise 4
  
  
  public  Author[] getAuthors() {return this.authors;} //public  Author getAuthor() {return this.author;} 
  public   double getPrice() {return this.price;}
  public  void setPrice( double price){this.price=price;} 
  public  double getQtyInStock() {return this.qtyInStock;}
  public  void setQtyInStock( int qtyInStock){this.qtyInStock=qtyInStock;} 
   
   public String toString(){
		return this.name +" by "+ authors.length+" authors.";
		//return this.name +" by "+getAuthor();
	}
}


class AuthorTest {
	public static void main(String[] args){
		Author[] authors=new Author[2]; // Author author= new Author("Malita Alin","malitaalin@email.com",'m');
		 authors[0] = new Author("Malita Alin","malitaalin@email.com",'m');
		 authors[1] = new Author("Margaret Atwood","emailname@email.com",'f');
		Book book = new Book("War and Peace", authors ,50.2); // Book book = new Book("War and Peace", author ,50.2);
		Book book1 = new Book("Day of Peace", authors ,40.2, 55);
		book.setPrice(33.44);
		book.setQtyInStock(4);
		System.out.println("Authors :"+book.getAuthors()[0]+" and "+book.getAuthors()[1]); 
		//System.out.println("Author :"+book.getAuthor()); 
		System.out.println("Name :"+book.getName()); 
		System.out.println("Price :"+book.getPrice()); 
		System.out.println("Qunatity :"+book.getQtyInStock());
		book.printAuthors();
		System.out.println(book);
		
	}
}