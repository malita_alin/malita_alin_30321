package Lab8;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
//given test code with necessary "throws IOException'
public class HomeAutomation {
	 
    public static void main(String[] args) throws IOException{

        //test using an anonymous inner class 
        Home h = new Home(){
            protected void setValueInEnvironment(Event event){
               System.out.println("New event in environment "+event); 
            }
            protected void controllStep(){
                System.out.println("Control step executed");
            }
        };
        h.simulate();
    }
}

abstract class Home {
	
   private Random r = new Random();
   private final int SIMULATION_STEPS = 20;
   private HouseSystem hs;

   protected abstract void setValueInEnvironment(Event event);
   protected abstract void controllStep();

   private Event getHomeEvent(){
       //randomly generate a new event;  
       int k = r.nextInt(100);
       if(k<30)
           return new NoEvent();
       else if(k<60)
           return new FireEvent(r.nextBoolean());
       else
           return new TemperatureEvent(r.nextInt(50));              
   }   

   public void simulate() throws IOException{
       int k = 0;
       hs=new HouseSystem();
       while(k <SIMULATION_STEPS){
           Event event = this.getHomeEvent();
           setValueInEnvironment(event);
           if(event.getType()==EventType.TEMPERATURE)
           	hs.Response(event,((TemperatureEvent) event).getVlaue(),false);
           if(event.getType()==EventType.FIRE) 
           	hs.Response(event,100,((FireEvent) event).isSmoke());
           controllStep();

           try {
               Thread.sleep(1000);
           } catch (InterruptedException ex) {
              ex.printStackTrace();
           }

           k++;
      }
   }
}
 
abstract class Event {
    EventType type;
 
    Event(EventType type) {
        this.type = type;
    }
 
    EventType getType() {
        return type;
    }
 
}
 
class TemperatureEvent extends Event {
 
    private int vlaue;
 
    TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }
 
    int getVlaue() {
        return vlaue;
    }
 
    @Override
    public String toString() {
        return "TemperatureEvent{" + "vlaue=" + vlaue + '}';
    }        
 
}
 
class FireEvent extends Event {
 
    private boolean smoke;
 
    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }
 
    boolean isSmoke() {
        return smoke;
    }
 
    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }
 
}
 
class NoEvent extends Event{
 
    NoEvent() {
        super(EventType.NONE);
    }    
 
    @Override
    public String toString() {
        return "NoEvent{}";
    }   
}
 
enum EventType {
    TEMPERATURE, FIRE, NONE;
}


/* New Classes - 
 HouseSystem = used for receiving events with method Response, then dealing with the Event accordingly
  ControlUnit= Singleton
   Alarm and GSM have simple toString methods
   TempDevice passes temperature to all its subclasses
   which have specific toString methods.*/
class HouseSystem{
	private Random r = new Random();
	ControlUnit control;
	Alarm alarm;
	GSM gsm;
	TemperatureSensor temperaturesensor; //1 Temp.Sensor
	FireSensor[] firesensor=new FireSensor[4]; //4 Fire-sensors
	CoolingUnit cool;
	HeatingUnit heat;
	

	public void Response(Event event, int temperature,boolean smoke) throws IOException {
		alarm=new Alarm();
		gsm=new GSM();
		temperaturesensor=new TemperatureSensor(temperature);
		cool=new CoolingUnit(temperature);
		heat=new HeatingUnit(temperature);
		
		
		if(event.getType()==EventType.TEMPERATURE) {  // EventType TEMPERATURE
			if(temperature>=25) {  
				ControlUnit.Control(temperaturesensor.toString(),cool.toString()); 
			}
			else if(temperature <25) {
				ControlUnit.Control(temperaturesensor.toString(),heat.toString());
			}
		 
		else if(event.getType()==EventType.FIRE) {//EventType FIRE
			String joined="";
			if(smoke==false) {   
				for(int i=0;i<4;i++){
					firesensor[i]=new FireSensor(temperature,smoke);
					joined=joined.concat(firesensor[i].toString(i)+"\n");
				}
				ControlUnit.Control(temperaturesensor.toString(), joined);
			}
			else { //smoke=true
				int x=r.nextInt(0,3); //random sensor picked
				for(int i=0;i<4;i++) 
					if(i!=x) {
						firesensor[i]=new FireSensor(temperature,false);
						joined=joined.concat(firesensor[i].toString(i)+"\n");
					}
				firesensor[x]=new FireSensor(temperature,true);
				joined=joined.concat(firesensor[x].toString(x)+"\n"+alarm.toString()+"\n"+gsm.toString());
				ControlUnit.Control(temperaturesensor.toString(), joined);}
				}		
		}	
	} }



class ControlUnit{//Singleton  = class containing a private constructor and a static method with a return type the same as the Singleton
	 static File output = new File("system_logs.txt");
	 
	 private static ControlUnit single_instance = null;
	 
	 private ControlUnit() {};
	 public static ControlUnit Control(String state,String command) throws IOException {
		 if (single_instance == null)
	            single_instance = new ControlUnit();
		 	FileWriter filewriter = new FileWriter(output, true);
		 	BufferedWriter out = new BufferedWriter(filewriter);
			out.write(state+"\n"+command+"------"+java.time.LocalDateTime.now()+"\n\n"); //command stored in file
			System.out.println(command);//command is printed in console
			out.close();
	        return single_instance;
	 }
}

class GSM{
	 public GSM() {};
	 public String toString() {
		 return "Owner called.";
	 }
}


class Alarm {
	public Alarm() {};
	public String toString() {
		return "Alarm is set off!";
	}
}

abstract class TempDevice{ //Abstract class TempDevice for passing temperature
	 private int temperature;
	 public TempDevice(int temperature){
		 this.temperature=temperature;
	 }
	 
	 int getTemperature() {
		 return temperature;
	 }
}



class FireSensor extends TempDevice{
	 boolean smoke;
	public FireSensor(int temperature,boolean smoke) {
		super(temperature);
		this.smoke=smoke;
	}

	public boolean getSmoke() {
		return smoke;
	}
	
	
	public String toString(int i) { //toString if clause based on smoke boolean value
		if(smoke==true)
			return "Smoke detected by sensor nr"+i;
		else
			return "No smoke detected by sensor nr"+i;
	} 
}

class TemperatureSensor extends TempDevice{
	 public TemperatureSensor(int temperature) {
		super(temperature);
	} 
	public String toString() {
		return "Temperature is:"+this.getTemperature();
	}
}

class HeatingUnit extends TempDevice{
	 public HeatingUnit(int temperature) {
		super(temperature);
	}
	
	public String toString() {
		return "Temperature is too low, HeatingUnit will start";
	}
}

class CoolingUnit extends TempDevice{
	 public CoolingUnit(int temperature) {
		super(temperature);
	}
	 public String toString() {
		 return "Temperature is too high, CoolingUnit will start ";
	 } 
}


