package Lab5;

abstract class Sensor {
	private String location;
	abstract int readValue();
	 public String getLocation() {return location;}
}

class TemperatureSensor extends Sensor{
	@Override
	 protected int readValue() {return (int)(Math.random()*100);};
}

class LightSensor extends Sensor{
	@Override
	protected int readValue() {return (int)(Math.random()*100);};
}


/*class Controller {
	public LightSensor lightSensor=new LightSensor();
	public TemperatureSensor tempSensor= new TemperatureSensor();
	public void control(){
		for(int i=1; i<=20; i++) {
			System.out.println("TempSensor= "+tempSensor.readValue()+" and LightSensor= "+ lightSensor.readValue());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {	// TODO Auto-generated catch block
				e.printStackTrace();}
		}
	}	
	} */ //Exercise 3 -  class Controller 

 class Controller {
	 
    private static Controller logger;
    public LightSensor lightSensor=new LightSensor();
	public TemperatureSensor tempSensor= new TemperatureSensor();
// Prevent clients from using the constructor
    private Controller() {
    }
 
//Control the accessible (allowed) instances
    public static Controller getFileLogger() {
        if (logger == null) {
            logger = new Controller();
        }
        return logger;
    }
    
    public void control(){
		for(int i=1; i<=20; i++) {
			System.out.println("TempSensor= "+tempSensor.readValue()+" and LightSensor= "+ lightSensor.readValue());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {	// TODO Auto-generated catch block
				e.printStackTrace();}
		}
	}	
}  //Exercise 4 : Singleton Variant 1 of class Controller



public class Exercise3 {
	public static void main(String[] args){
		// Exercise 3 object initialisation:   Controller controller=new Controller();
		//Exercise 3 control method call :   controller.control();
		Controller.getFileLogger().control(); //Exercise 4 
	}
}
