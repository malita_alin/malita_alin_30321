package Lab5;

 interface Image {
	   abstract void display();
	}
	 
class RealImage implements Image {
	 
	   private String fileName;
	 
	   public RealImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	 
	   @Override
	   public void display() {
	      System.out.println("Displaying " + fileName);
	   }
	 
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	}
	 
 class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private RotatedImage rotatedImage;
	   private String fileName;
	   private boolean choice;
	 
	   public ProxyImage(String fileName){
	      this.fileName = fileName;
	      this.choice=false;
	   }
	   public ProxyImage(String fileName,boolean choice){
		      this.fileName = fileName;
		      this.choice=choice;
		   }
	 
	    public void setChoice(boolean choice) {this.choice=choice;}
	   @Override
	   public void display() {
		   if(choice) {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();}
		   else {
	      if(rotatedImage == null){
		         rotatedImage = new RotatedImage(fileName);
		      }
		      rotatedImage.display();
		   } 
	   
	} }
 
 
 class RotatedImage implements Image {
	 
	   private String fileName;
	 
	   public RotatedImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	 
	   @Override
	   public void display() {
	      System.out.println("Display rotated " + fileName);
	   }
	 
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	}
 
public class Exercise2 {
	public static void main(String[] args){
		 //No UML diagram
		 RealImage image1= new RealImage("Image1");
		 image1.display();
		 RotatedImage image2= new RotatedImage("Image2");
		 image2.display();
		 ProxyImage image3= new ProxyImage("Image3");
		 image3.display();
		 image3.setChoice(true);
		 image3.display();
		 ProxyImage image4= new ProxyImage("Image4",true);
		 image4.display();
	 }	 
}
