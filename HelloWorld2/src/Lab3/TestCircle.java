package Lab3;

public class TestCircle {
	
	public static void main(String[] args){
		Circle circle1 = new Circle(4);
		Circle circle2 = new Circle(5,"blue");
		System.out.println("Radius circle1: "+circle1.getRadius());
	   System.out.println("Color circle1: "+circle1.getColor());
	   System.out.println("Radius circle2: "+circle2.getRadius());
	   System.out.println("Color circle2: "+circle2.getColor());
	}
}
