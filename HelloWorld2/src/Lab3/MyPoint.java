package Lab3;

public class MyPoint {
	int x;
	int y;
	
	 MyPoint(){
		this.x=0;
		this.y=0;
	}
	
	MyPoint(int x, int y){
		this.x=x;
		this.y=y;	
	}
	
	int getX() {return x;}
	void setX(int x) {this.x=x;}
	int getY() {return y;}
	void setY(int y) {this.y=y;}
	
	 public String toString(){
	 		return "Point Coordinates = ("+x+","+y+")";
	 	}
	 
	 
	 public double distance(int x, int y) {
		 return Math.sqrt(Math.pow(this.x-x,2)+Math.pow(this.y-y, 2));
	 }
	 public double distance(MyPoint another) {
		 return Math.sqrt(Math.pow(this.x-another.x,2)+Math.pow(this.y-another.y, 2));
	 }
}
