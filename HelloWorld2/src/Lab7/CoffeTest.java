package Lab7;

public class CoffeTest {
    public static void main(String[] args) {
          CofeeMaker mk = new CofeeMaker();
          CofeeDrinker d = new CofeeDrinker();

          for(int i = 0;i<15;i++){
                Cofee c = mk.makeCofee(i+1);
                try {
                	mk.makeCofe(c);
                      d.drinkCofee(c);
                } catch (TemperatureException e) {
                      System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
                } catch (ConcentrationException e) {
                      System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
                }
               catch (QuantityException e) {
              System.out.println("Exception:"+e.getMessage()+"- coffees made="+e.getQuantity()); //added exception catch
                }
                finally{
                      System.out.println("Throw the cofee cup.\n");
                }
          }    
    }
}//.class

class CofeeMaker {
    Cofee makeCofee(int q){
          System.out.println("Make a coffe");
          int t = (int)(Math.random()*100);
          int c = (int)(Math.random()*100);
          Cofee cofee = new Cofee(t,c,q);
          return cofee;
    }

    void makeCofe(Cofee c) throws QuantityException{
    	if(c.getQuantity()>10) {
    		throw new QuantityException(c.getQuantity(), "No cups left");  //added exception condition + message 
    	}
    }
}//.class

class Cofee{
    private int temp;
    private int conc;
    private int number;
    
    Cofee(int t,int c,int q){temp = t;conc = c; number=q;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getQuantity() {return number;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+": order number="+number+"]";}
}//.class

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException{
          if(c.getTemp()>60)
                throw new TemperatureException(c.getTemp(),"Cofee is too hot!");
          if(c.getConc()>50)
                throw new ConcentrationException(c.getConc(),"Cofee concentration too high!");     
  
          System.out.println("Drink cofee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
          super(msg);
          this.t = t;
    }

    int getTemp(){
          return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
          super(msg);
          this.c = c;
    }

    int getConc(){
          return c;
    }
}//.class

class QuantityException extends Exception{
	 int quantity;
	    public QuantityException(int q,String msg) {
	          super(msg);
	          this.quantity=q;
	    }

	    int getQuantity(){
	          return quantity;
	    }
	    void setQuantity(int q) {
	    	this.quantity = q;
	    }
}  //added exception