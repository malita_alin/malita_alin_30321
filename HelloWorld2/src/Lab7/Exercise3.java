package Lab7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercise3 {
	public static void Encrypt(File f, String filename) throws IOException{
		 FileWriter file=new FileWriter("D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+filename+".enc");     
	     FileReader fr=new FileReader(f);
	     BufferedReader br=new BufferedReader(fr);
	     int c = 0;         
		while((c = br.read()) != -1)        
	     {
	           file.write(c+1);
	     }
		file.close();
	}
	
	public static void Decrypt(File f, String filename) throws IOException{
		 FileWriter file=new FileWriter("D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+filename+".dec");      
	     FileReader fr=new FileReader(f);
	     BufferedReader br=new BufferedReader(fr);
	     int c = 0;         
		while((c = br.read()) != -1)        
	     {
	           file.write(c-1);
	     }
		file.close();
	}
	
	public static void main(String[] args) throws IOException 
	{
	
	 File f; 
     Scanner in=new Scanner(System.in);
     Scanner sc=new Scanner(System.in);
     int choice=-1;
     String fileName;
     String filePath;
 	//Switch Menu
 	 	while(choice!=5) {
 	 		System.out.println("Enter option:\n1.Encrypt file\n2.Decrypt file\n3.Exit\n");
 	 	 	choice=in.nextInt();
 	 	switch(choice) {
 	 	case 1:
 	 		System.out.println("Enter the name of the file to be encrypted ");
 	 		fileName=sc.nextLine();
 	 		filePath="D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+fileName+".txt"; 
 	 		//another option is to have either a menu to choose type of file, or to simply put the file extension in fileName
 	 		f=new File(filePath);
     Encrypt(f,fileName);
     break;
 	 	case 2:
 	 		System.out.println("Enter the name of the file to be decrypted ");
 	 		fileName=sc.nextLine();
 	 		filePath="D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+fileName+".enc"; 
 	 		//this considers that we input a file that is encrypted/has extension enc. 
 	 		/*D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7 is the fpath to the folder we access 
 	 	 for the original file and in which we create encrypted and decrypted versions. It could be replaced with another variable that can be inputed,
 	 		 or it could all be stored in fileName*/
 	 		f=new File(filePath);
            Decrypt(f,fileName);
     break;
      	case 3:
 	 		return;
 	 	default: 
 	 	System.out.print("Wrong input.Try to input a valid option: ");
 		break;
  }
}
	}
	
}
