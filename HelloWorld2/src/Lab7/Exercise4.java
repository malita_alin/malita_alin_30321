package Lab7;
import java.util.*;
import java.io.*;

public class Exercise4 {

      public static void main(String[] args) throws Exception{
            CarFactory factory = new CarFactory();
            
            File f1; 
            Scanner in=new Scanner(System.in);
            Scanner sc=new Scanner(System.in);
            int choice=-1;
            int count=0;
            String inputModel;
            double inputPrice;
            int inputId;
            String fileName;
            String filePath;

            Car cars[]=new Car[100]; //array for the cars 
        	//Switch Menu
        	 	while(choice!=6) {
        	 		System.out.println("Enter option:\n1.Create Car\n2.Store a Car in a file\n3.Retrieve Car from file\n4.Read Car info from a file\n5.Exit\n");
        	 	 	choice=in.nextInt();
        	 	 	
        	 	switch(choice) {  ///make so you can input objects, also fix save and retrieve in same file
        	 	case 1:
        	 		count++;
        	 		System.out.println("Input the model of the new car");
        	 		inputModel=sc.nextLine();
        	 		System.out.println("Input the price of the new car");
        	 		inputPrice=in.nextDouble();
        	 		 cars[count] = factory.createCar(inputModel,inputPrice,count);
        	 		break;
        	 	case 2:
        	 		System.out.println("Enter the name of the file ");
        	 		fileName=sc.nextLine();
        	 		filePath="D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+fileName+".txt"; 
        	 		//another option is to have either a menu to choose type of file, or to simply put the file extension in fileName
        	 		f1=new File(filePath);
        	 		System.out.println("Input the Id of the car to be stored");
        	 		inputId=in.nextInt();
        	 		 factory.storeCar(cars[inputId],filePath);
            break;
        	 	case 3:
        	 		System.out.println("Enter the name of the file ");
        	 		fileName=sc.nextLine();
        	 		filePath="D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+fileName+".txt"; 
        	 		f1=new File(filePath);
        	        Car x=factory.retrieveCar(filePath);
        	        System.out.println(x);
            break;
        	 	case 4:
        	 		System.out.println("Enter the name of the file ");
        	 		fileName=sc.nextLine();
        	 		filePath="D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\"+fileName+".txt"; 
        	 		f1=new File(filePath);
        	        factory.retrieveCar(filePath);
        	 		break;
             	case 5:
        	 		return;
        	 	default: 
        	 	System.out.print("Wrong input.Try to input a valid option: ");
        		break;
         }
       }
      }
}//.class
 
class CarFactory{
      Car createCar(String model, double price,int id){
            Car z = new Car(model, price,id);
            System.out.println(z);
            return z;
      }
 
      void storeCar(Car a, String storageFile) throws IOException{
            ObjectOutputStream o =
              new ObjectOutputStream(
                new FileOutputStream(storageFile));
            o.writeObject(a);
            System.out.println(a+" has been stored.");
      }
 
     Car retrieveCar(String storageFile) throws IOException, ClassNotFoundException{
           
    	  try {  String line;
    		  ObjectInputStream in =
                    new ObjectInputStream(
                      new FileInputStream(storageFile));
              Car x = (Car)in.readObject();
             System.out.println(x+" retrieved.");
             return x;
               } 
           catch (IOException e) {
               System.err.println("There is no such file.");
               }
    	  System.out.println(" No objects");
    	  return null;
	
      }
}//.class
 
 
class Car implements Serializable{
      private String model;
     private  double price=0;
     private int id;
     
      public Car(String model, double price, int id) {
            this.model = model;
           this.price =  price;
           this.id=id;
      }
      public String toString(){return "[Car of id= "+id+" - model= "+model+" - price="+price+"]";}
}//.class