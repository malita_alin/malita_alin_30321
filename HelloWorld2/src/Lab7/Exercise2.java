package Lab7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner; 

  
public class Exercise2 {
	public static void main(String[] args) throws IOException 
	{
	 File f=new File("D:\\School Books\\An II Semestru II\\SE\\eclipse-workspace\\HelloWorld2\\src\\Lab7\\ex2filetoread.txt");      
     FileReader fr=new FileReader(f);  
     BufferedReader br=new BufferedReader(fr);  
     int c = 0;         
     int counter = 0;
     Scanner s= new Scanner(System.in);
     System.out.println("Enter the letter to count ");
     char chr = s.next().charAt(0);
     while((c = br.read()) != -1)        
     {
           char character = (char) c;          
           if(character == chr) counter++;      
     }
     System.out.println("Letter '"+chr+"' count="+counter);
     
  }
}
