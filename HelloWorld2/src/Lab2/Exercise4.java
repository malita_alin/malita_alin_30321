package Lab2;

import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args)
    {
    	Scanner in= new Scanner(System.in);
    	System.out.print("Enter the number of elements you want to store: ");
    	//reading the number of elements from the that we want to enter  
    	int n=in.nextInt();  
    	//creates an array in the memory of length 10  
    	int[] array = new int[10];  
    	System.out.println("Enter the elements of the vector: ");  
    	for(int i=0; i<n; i++)  
    	{  
    	//reading array elements from the user   
    	array[i]=in.nextInt();  
    	}  
    	
    	int vmax=0;
    	System.out.println("Vector elements are: ");  
    	// accessing array elements using the for loop  
    	for (int i=0; i<n; i++)   
    	{  
    	System.out.println(array[i]); 
    	if(array[i] > vmax) { vmax=array[i];}
    	}  
    	
        System.out.println(
            "The maximum value present in Vector is : " + vmax);
    }
}
