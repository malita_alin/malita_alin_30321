package Lab2;

import java.util.Scanner;

public class Exercise3 {
	public static void main(String[] args){
	       Scanner in= new Scanner(System.in);
	       System.out.println("Input A=");
	       int A = in.nextInt();
	       System.out.println("Input B=");
	       int B = in.nextInt();
	       
	       int max = 0;
	       if(A>B) {max= A ;}else{ max= B;}
	       int min = 0;
	       if(A<B) {min= A ;}else{ min= B;}
	      int primenr=0;
	      boolean isPrime = true;
	       for(int i = min; i<=max ; i++) {
	    	   isPrime = true;
	    	   if (i == 1 || i == 0)// Skip 0 and 1 as they are neither prime nor composite
	                continue;
	    	   for (int j = 2; j<= i/2; j++)	  
	    	   {if (i % j == 0)
	    	   {     isPrime = false; break; }
	    	   }
	    	   if (isPrime == true) {primenr++; System.out.println("Prime nr found: "+i);}
	    	 }
	   	System.out.println("Between "+min+" and "+max+" there are "  +primenr+" prime numbers");  
	}
}
