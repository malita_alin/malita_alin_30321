package Lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise5 {
	   public static void main(String[] args)
	    {
	Scanner in= new Scanner(System.in);
	Random rd = new Random(); // creating Random object
	int n=10;
	int[] array = new int[n];
	 System.out.println("The Unsorted Vector elements are: ");
    for (int i = 0; i < array.length; i++) {
       array[i] = rd.nextInt(100 - (-100))-100; // storing random integers in an array 
       //We can change the range of the random variables using the values inside nextInt()
       //-100= min is added
       System.out.println(array[i]); // printing each array element
    }
    
    int temp=0; //bubble sort
    for (int i = 0; i < n-1; i++)
        for (int j = 0; j < n-i-1; j++)
            if (array[j] > array[j+1])
            {
                // swap arr[j+1] and arr[j]
                temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
    
	System.out.println("The Sorted Vector elements are: ");  //display sorted array
	for (int i=0; i<n; i++)   
	{  
	System.out.println(array[i]); 
	}  
} }
