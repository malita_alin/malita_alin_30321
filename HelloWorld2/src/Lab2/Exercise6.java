package Lab2;

import java.util.Scanner;

public class Exercise6 {
	  public static void main(String[] args)
	    {
	Scanner in= new Scanner(System.in);
	System.out.print("Enter the number: ");
	//reading the number of elements from the that we want to enter  
	int n=in.nextInt(); 
	//non-recursive method
	long fact = 1;
    for (int i = 2; i <= n; i++) {
        fact = fact * i;
    }
    System.out.println(
            "The factorial value of "+n+" using for loop is: " +fact);
   
    int recfact= factorialUsingRecursion(n);
    System.out.println(
            "The factorial value of "+n+" using recursion is: " +recfact);
	    }
	  //recursive method 
	  public static int factorialUsingRecursion(int n){
		    if (n <= 2) {
		        return n;
		    }
		    return n * factorialUsingRecursion(n - 1);
		}
	 
}