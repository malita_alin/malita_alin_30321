package Lab11;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class Exercise2 extends JFrame{

	private JLabel lblName, lblQuantity, lblPrice;
	   private JButton addProduct, viewProducts, deleteProduct,changeQuantity;   
	   private JButton bExit; 
   private JTextField productName, productQuantity, productPrice;
   private JTextArea display;
   
   public ArrayList<Product> products=new ArrayList<Product>();
   private static int counter;

 private Exercise2(){
 	 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setTitle("App - Stock Management"); 
     setLayout(null);
     counter=0;
     init();
     setSize(800, 450);        
	   setVisible(true);  
 }
 
 public void init(){

	 
    JPanel panel=new JPanel();  
    panel.setLayout(new FlowLayout() );

     
	 lblName =new JLabel("Product Name:");
	 lblName.setFont(new Font("Arial",Font.BOLD, 14));
     panel.add(lblName);  
     
     productName = new JTextField(25); 
     productName.setEditable(true);       
     panel.add(productName);  
     
	 lblQuantity =new JLabel("Quantity Name:");
	 lblQuantity.setFont(new Font("Arial",Font.BOLD, 14));
	 panel.add(lblQuantity); 
	 
     productQuantity = new JTextField(8); 
     productQuantity.setEditable(true);       
     panel.add(productQuantity);  
     
     
	 lblPrice=new JLabel("Price Name:");
	 lblPrice.setFont(new Font("Arial",Font.BOLD, 14));
	 panel.add(lblPrice); 
	 
	    productPrice = new JTextField(10); 
	     productPrice.setEditable(true);       
	     panel.add(productPrice);  
	     
     
	 addProduct=new JButton("Add Product");
	 panel.add(addProduct);
	 addProduct.addActionListener(new AddButton());
	 
	 deleteProduct=new JButton("Delete Product");
	 deleteProduct.addActionListener(new DeleteButton());
	 panel.add(deleteProduct);
	 
	 viewProducts=new JButton("View Products");
	 viewProducts.addActionListener(new ViewButton());
	 panel.add(viewProducts);
     
	 changeQuantity=new JButton("Update Quantity");
	 changeQuantity.addActionListener(new UpdateButton());
	 panel.add(changeQuantity);
	 
	 
	 bExit = new JButton("Restart/Quit");
     bExit.addActionListener(new ExitButton());
     panel.add(bExit);
	
	 display =new JTextArea(70,70);
     display.setEditable(false);
	 panel.add(display);
	 

     
     this.setContentPane(panel);
     this.pack();

 }
 
 
 class AddButton implements ActionListener{
     public void actionPerformed(ActionEvent e){
         Product temp=new Product();
         temp.add(productName.getText(),parseInt(productQuantity.getText()),parseFloat(productPrice.getText()));
         products.add(counter++,temp);
         display.setText("Product: "+productName.getText()+" added.");
     }
 }

 class DeleteButton implements ActionListener{
     public void actionPerformed(ActionEvent e){
         String name=productName.getText();
         int found=0;
         int index;
         for(index =0; index <counter; index++)
             if(products.get(index).checkName(name)) {
                 found=1;
                 break;
             }
         if(found==1)
         {    display.setText("Product: "+productName.getText()+" deleted.");
             products.remove(index);
         }
     }
 }

 class ViewButton implements ActionListener{
     public void actionPerformed(ActionEvent e){
         display.setText(products.toString());
     }
 }

 class UpdateButton implements ActionListener{
     public void actionPerformed(ActionEvent e){
    	  int index;
         String pName=productName.getText();
         int found=0;
         for(index =0; index <counter; index++)
             if(products.get(index).checkName(pName)) {
                 found=1;
                 break;
             }
         if(found==1){
             Product temp=new Product();
             temp.add(productName.getText(),parseInt(productQuantity.getText()),products.get(index).getPrice());
             products.set(index,temp);
             display.setText("Product's Quantity: "+productName.getText()+" updated.");
         }
     }
 }



 class ExitButton implements ActionListener{
	  @Override
	  public void actionPerformed(ActionEvent e) {
		  Object[] option={"Restart","Quit"};
	         int n=JOptionPane.showOptionDialog(null, "Restart or Quit App\n","Restart/Exit",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,option,option[0]);
	         if(n==0){
	             dispose();
	             new Exercise2();
	         }
	         else{
	             dispose();
	         }
		}
	 }
 
 public static void main(String[] args) {
    JFrame exercise = new Exercise2();
}
 }


class Product {
	private String name;
	private int quantity;
	private float price;
	
	public Product(){
        name="";
        quantity=0;
        price=0;
    }
	
   
    public void add(String name,int quantity, float price){
        this.name=name;
        this.quantity=quantity;
        this.price=price;
    }
    
    //search if product exists by name
    public Boolean checkName(String name){
        if(this.name.equals(name))
            return true;
        else
            return false;
    }

    public float getPrice(){
        return price;
    }

    //view product
    public String toString(){
            return "Product: "+ name+"/ Quantity: "+quantity+"/ Price "+price+"\n";
    }

   

}
