package Lab11;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class xercise2{
    public static void main(String[] Args){
        JFrame presentation=new StockManagement();
        presentation.setVisible(true);
    }
}

class StockManagement extends JFrame{
    ///Model
    public ArrayList<Products> products=new ArrayList<Products>();
    private static int number;

    ///Input fields
    private JTextField pName=new JTextField(30);
    private JTextField pQuantity=new JTextField(10);
    private JTextField pPrice=new JTextField(10);

    ///Display area
    private JTextArea displayArea=new JTextArea(100,60);

    ///Buttons for actions
    private JButton Padd=new JButton("Add product");
    private JButton Pdelete=new JButton("Delete product");
    private JButton Pchange=new JButton("Change product quantity");
    private JButton Pdisplay=new JButton("Display products");

    public StockManagement(){
        number=0;

        ///Components initialisation
        displayArea.setEditable(false);

        ///Layout components
        JPanel content=new JPanel();
        content.setLayout(new FlowLayout());
        content.add(new JLabel("Products name"));
        content.add(pName);
        content.add(new JLabel("Quantity"));
        content.add(pQuantity);
        content.add(new JLabel("Price"));
        content.add(pPrice);
        content.add(Padd);
        content.add(Pdelete);
        content.add(Pchange);
        content.add(Pdisplay);
        content.add(displayArea);

        ///Button listeners
        Padd.addActionListener(new AddListener());
        Pdelete.addActionListener(new DeleteListener());
        Pchange.addActionListener(new ChangeListener());
        Pdisplay.addActionListener(new DisplayListener());

        ///final layout
        this.setContentPane(content);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Stock Management");
    }
    ///inner class to add a product
    class AddListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Products aux=new Products();
            aux.add(pName.getText(),parseInt(pQuantity.getText()),parseFloat(pPrice.getText()));
            products.add(number++,aux);
        }
    }

    ///inner class to display products
    class DisplayListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            displayArea.setText(products.toString());
        }
    }

    ///inner class for deleting products
    class DeleteListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String name=pName.getText();
            int found=0;
            int index;
            for(index =0; index <number; index++)
                if(products.get(index).sameName(name)) {
                    found=1;
                    break;
                }
            if(found==1)
                products.remove(index);
        }
    }

    ///inner class for changing quantity
    class ChangeListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String name=pName.getText();
            int found=0;
            int index;
            for(index =0; index <number; index++)
                if(products.get(index).sameName(name)) {
                    found=1;
                    break;
                }
            if(found==1){
                Products aux=new Products();
                aux.add(pName.getText(),parseInt(pQuantity.getText()),products.get(index).getPrice());
                products.set(index,aux);
            }
        }
    }
}

class Products{

    private String name;
    private int quantity;
    private float price;

    public Products(){
        name="";
        quantity=0;
        price=0;
    }
    ///add
    public void add(String name,int quantity, float price){
        this.name=name;
        this.quantity=quantity;
        this.price=price;
    }

    ///view product
    public String toString(){
            return name+" quantity: "+quantity+" price "+price+"\n";
    }

    ///find product by name
    public Boolean sameName(String name){
        if(this.name.equals(name))
            return true;
        else
            return false;
    }

    public float getPrice(){
        return price;
    }
}