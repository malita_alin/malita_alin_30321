package Lab11;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;


public class Exercise {
    public static void main(String[] args) throws InterruptedException{
        Temperature temperatureSensor = new Temperature();
        TSensor monitor = new TSensor();
       temperatureSensor.register(monitor);
        temperatureSensor.measure();

    }
}

class Temperature extends Observable{
    float temperature;

    public void measure() throws InterruptedException {
        while(true){
            temperature=(float) (Math.random()*45);
            this.changeState(temperature);
            Thread.sleep(1100);
        }
    }
}

class TSensor extends JFrame implements Observer{
    JTextArea tArea=new JTextArea();

    public TSensor(){
        setTitle("Temperature Sensor");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,200);
        tArea=new JTextArea();
        add(tArea);
        tArea.setEditable(false);
        setVisible(true);
    }
    
    @Override
    public void update(Object event) {
        tArea.setText("Sensor reads: "+event.toString()+"C");

    }
}


