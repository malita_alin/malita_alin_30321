package Lab12.vehicles2.main;

public class ElectricVehicle extends Vehicle {
    public ElectricVehicle(String type, int length) {
        super(type, length);
    }
    public String start(){
        return "electric engine started";
    } //added start method that simply returns String message
}
