package Lab12.vehicles2.main;

import java.util.Objects;

public class Vehicle {

    private String type;
    private int weight;

    public Vehicle(String type, int length) {
        this.type = type;
        this.weight=length;
    }

    public String start(){
        return "engine started";
    }
    public int getWeight(){
        return weight;
    }

    public String getType(){return type;}
    @Override
    public boolean equals(Object obj) {
        if(this==obj)
            return true;
        else{
            Vehicle vehicle=(Vehicle) obj;
            return weight== vehicle.getWeight() && type.equals(vehicle.type);
        }
    } //overriden equals method (checks both weight and type)

    @Override
    public int hashCode() {
        return Objects.hash(type,weight);
    }  //override hashcode
}
