package Lab12.vehicles2.main;

public class ElectricBattery {

    /**
     * Percentage load.
     */
    private int charge = 0;

    public void charge() throws BatteryException {
        if(charge<=100)
            charge++;
        else
            throw new BatteryException(ElectricBattery.class.toString());
    }
  //add exception and charge limit
}