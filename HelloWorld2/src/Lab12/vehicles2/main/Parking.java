package Lab12.vehicles2.main;

import java.util.ArrayList;

public class Parking {
    private static int index=0;
 //added Vehicle arraylist
    ArrayList<Vehicle> parkedVehicles=new ArrayList<>();
    public void parkVehicle(Vehicle e){
        parkedVehicles.add(index++,e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
        for(int i=0;i<index-1;i++)
            for(int j=i+1;j<index;j++)
                if(parkedVehicles.get(i).getWeight()>parkedVehicles.get(j).getWeight())
                {   Vehicle temp=parkedVehicles.get(i);
                    parkedVehicles.set(i,parkedVehicles.get(j));
                    parkedVehicles.set(j,temp);
                }

    }  //same sortByWeight method created for vehicles package

    public Vehicle get(int index){
        return parkedVehicles.get(index);
    }
    //same get Vehicle method created for vehicles package
}