package Lab12.electronic_device.test;

import static org.junit.Assert.*;
import Lab12.electronic_device.main.ElectronicDevice;
import org.junit.Test;

public class EectronicDeviceTest {

    @Test
    public void testCreate(){
        ElectronicDevice device=new ElectronicDevice();
        assertFalse(device.isPowered());
    }

    @Test
    public void testPowerState(){
        ElectronicDevice device=new ElectronicDevice();
        device.turnOn(); //made the turnOn and turnOff methods public
        assertTrue(device.isPowered());
        device.turnOff();
        assertFalse(device.isPowered());
    }
}
