package Lab12.electronic_device.test;

import Lab12.electronic_device.main.TV;
import static org.junit.Assert.*;
import org.junit.Test;


public class TVTest {

    @Test
    public void testCreate(){
        TV tv1=new TV();
        assertEquals(0,tv1.getChannel()); //new tv starts at channel 0
    }

    @Test
    public void testChannelChange(){
        //includes both channelUp() and channelDown() methods
        TV tv1=new TV();
        tv1.channelUp();
        tv1.channelUp();
        tv1.channelUp();
        tv1.channelUp();
        assertEquals(4,tv1.getChannel());
        tv1.channelDown();
        tv1.channelDown();
        assertEquals(2,tv1.getChannel());
    }
}
