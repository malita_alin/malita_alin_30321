package Lab12.electronic_device.main;

public class TV extends ElectronicDevice{
    int channel;

    public TV() {
    }

    public void channelUp(){
        if(channel<=10){
            channel++;
            System.out.println("Channel up "+channel);
        }
    }

    public void channelDown(){
        if(channel>=2){
            channel--;
            System.out.println("Channel down "+channel);
        }
    }

    public int getChannel() {
        return channel;
    }
}