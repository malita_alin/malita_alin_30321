package Lab12.vehicles.main;

import java.util.ArrayList;


public class Parking {

    /**
     * Vehicles will be parked in parkedVehicles array.
     */
    private static int index=0;

    ArrayList<Vehicle> parkedVehicles=new ArrayList<>();

    public void parkVehicle(Vehicle e){
        this.parkedVehicles.add(index++,e);
    }
    /**
     * Sort vehicles by length.
     */
    public void sortByWeight() { //bubblesort added
        for (int i = 0; i < index - 1; i++)
            for (int j = i + 1; j < index; j++)
                if (parkedVehicles.get(i).getWeight() > parkedVehicles.get(j).getWeight()) {
                    Vehicle temp = parkedVehicles.get(i);
                    parkedVehicles.set(i, parkedVehicles.get(j));
                    parkedVehicles.set(j, temp);
                }

    }

    public Vehicle get(int index) {
        return parkedVehicles.get(index);
    }  //proper get vehicle method by index - added
}

