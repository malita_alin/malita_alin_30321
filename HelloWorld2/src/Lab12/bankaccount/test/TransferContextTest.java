package Lab12.bankaccount.test;

import Lab12.bankaccount.main.BankAccount;
import Lab12.bankaccount.main.TransferContext;
import static org.junit.Assert.*;
import org.junit.Test;

public class TransferContextTest {

    @Test
    public void transfer() {
        BankAccount sender = new BankAccount("A01" ,100);
        BankAccount receive = new BankAccount("B02", 100);
        TransferContext context = new TransferContext();
        context.transfer(sender, receive, 10);
        assertEquals(90, sender.getBalance());
        assertEquals(110, receive.getBalance());
    }
}