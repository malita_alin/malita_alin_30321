package Lab12.bankaccount.main;


import java.util.ArrayList;
public class AccountsManager {
    private static int index=0;
    //set arraylist of BankAccounts
    ArrayList<BankAccount> bankAccounts=new ArrayList<BankAccount>();
    public void addAccount(BankAccount account){
        bankAccounts.add(index++,account);
    }
    //checks whether an account of given id exists or not
    public boolean exists(String id){
        boolean exist=false;
        for(int i=0;i<index;i++)
            if(bankAccounts.get(i).getId().equals(id))
                exist=true;
        return exist;
    }

    public int count(){
        return index; //previously returned just 0
    }
}
