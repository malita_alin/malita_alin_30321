package Lab12.bankaccount.main;

import java.util.logging.Logger;

public class BankAccount {

    public static Logger LOG = Logger.getLogger( BankAccount.class.getName() );

    private String id;
    private int balance;

    public BankAccount(String id, int balance) {
        this.id = id;
        this.balance = balance;
    }

//increase method now addd money to balance
    public void increase(int money) {
        this.balance=this.balance+money;
    }

    public void decrease(int money) {
        this.balance -= money;
    }

    public int getBalance() {
        return this.balance;
    }

    public String getId() {
        return this.id;
    }
//the rest of methods are correct as they originally are
}
