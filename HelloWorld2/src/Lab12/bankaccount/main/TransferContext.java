package Lab12.bankaccount.main;

public class TransferContext {
    private BankAccount sender;
    private BankAccount receiver;
    //create 2 BankAccount instances for the transfer
    public void transfer(BankAccount sender, BankAccount receiver, int ammount){
        this.sender=sender;
        this.receiver=receiver;
        sender.decrease(ammount);
        receiver.increase(ammount);
    } //simply decrease the ammount from sender and adding ( increase) it to receiver

}
