package Lab12.template.test;

import Lab12.template.main.ArithmeticOperationsUtils;
        import org.junit.Test;

        import static org.junit.Assert.assertEquals;

/**
 * @author Radu Miron
 * @version 1
 */
public class SimpleExampleTest {

    @Test
    public void testAdd() {
        assertEquals("ArithmeticOperation.add() is not working ", 23, ArithmeticOperationsUtils.add(11, 12));
    }

    @Test
    public void testSubstract() {
        // todo: fix it
        assertEquals("ArithmeticOperation.substract() is not working ", -1, ArithmeticOperationsUtils.substract(11, 12));
    } // used to be expected:-2, it could be change so that the function adds 1 after making the substraction but that is not logical

    @Test
    public void testMultiply() {
        assertEquals("ArithmeticOperation.multiply() is not working ", 132, ArithmeticOperationsUtils.multiply(11, 12));
    }

    @Test
    public void testDivide() {
        assertEquals("ArithmeticOperation.divide() is not working ", 0.9166666865348816, ArithmeticOperationsUtils.divide(11, 12), 0);
    }

}