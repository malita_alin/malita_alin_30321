package Lab12.template.main;

public class ArithmeticOperationsUtils {

    public static int add(int a, int b) {
        return a + b;
    }

    public static int substract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static float divide(int a, int b) {
        return a / (float) b;
    }
}
