package Lab6;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

class Dictionary{
	private HashMap<Word,Definition> dictionary=new HashMap<Word,Definition>();
	
	public void addWord(Word w, Definition d) {
		dictionary.put(w,d);
	}
	
	public Definition getDefinition(Word w) {
		return dictionary.get(w);
	}
	
	public void getAllWords() {
		Iterator<Word> iterator=dictionary.keySet().iterator();
		while(iterator.hasNext())
			System.out.println(iterator.next());
	}
	
	public void getAllDefinitions() {
		Iterator<Entry<Word, Definition>> iterator=dictionary.entrySet().iterator();
		while(iterator.hasNext())
			System.out.println(iterator.next());
	}
	
}

class Word{
	private String name;
	public Word(String name) {
		this.name=name;
	}
	
	public String toString() {
		return this.name;
	}
	public int hashCode() {
		return (int) (name.length()*1000);
	}
	
	public boolean equals(Object obj) {
		if(!(obj instanceof Word))
				return false;
		Word x=(Word)obj;
		return name.equals(x.name);
	}
}

class Definition{
	private String description;
	public Definition(String description) {
		this.description=description;
	}
	
	public String toString() {
		return this.description;
	}
	
	public boolean equals(Object obj) {
		if(!(obj instanceof Definition))
				return false;
		Definition x=(Definition)obj;
		return description.equals(x.description);
	}
}

public class ConsoleMenu {
	 public static void main(String[] args)
	 {	
			Scanner in=new Scanner(System.in);
	    Word word;
	 	Definition def;
	 	Dictionary dictionary=new Dictionary();
	 	int choice=0;
	//Switch Menu
	 	while(choice!=5) {
	 		System.out.println("Enter option:\n1.Add a Word\n2.Get Definition of a word\n3."
		 			+ "Get all words stored\n4.Get all definitions stored\n");
	 	 	choice=in.nextInt();
	 	switch(choice) {
	 	case 1:
	 		System.out.print("Word to add:");
	 		word=new Word(in.next());
	 		System.out.print("Definition of word:");
	 		def=new Definition(in.next());
	 		dictionary.addWord(word, def);
	 		
	 		break;
	 	case 2:	
	 		System.out.print("Input word for definition search: ");
	 		word=new Word (in.next());
	 		System.out.println(word+"= "+ dictionary.getDefinition(word));
	 		break;
	 	case 3:
	 		dictionary.getAllWords();
	 		break;
	 	case 4:
	 		dictionary.getAllDefinitions();
	 		break;
	 	default:
	 		System.out.print("Wrong input.Try to input a valid option: ");
	 		break;
	 	}
	 } 
	}
}