package Lab6;

 class BankAccount implements Comparable<BankAccount>{
private String owner="null";
private double balance=0;

public BankAccount(String owner, double balance) {
	this.owner=owner;
	this.balance=balance;
}
 public void withdraw(double amount) {this.balance=this.balance-amount;}
 public void deposit(double amount) {this.balance=this.balance+amount;}
public String getOwner() {return this.owner;}
public double getBalance() {return this.balance;}

 @Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount){
			BankAccount b = (BankAccount)obj;
			return this.owner  == b.owner;	}
		return false;
	}
 
 public int hashCode(){
     return this.owner.hashCode() + (int)this.balance;
 }
 
@Override
public int compareTo(BankAccount bankaccount) {
	 if(this.balance>bankaccount.balance){  
	        return 1;  
	    }else if(this.balance<bankaccount.balance){  
	        return -1;  
	    }else{  
	    return 0;  
	    }  
	}  

  public String toString() {
	  return this.owner+": "+this.balance;
  }  //method called for getAllAccounts() method in the next exercise
}


public class Exercise1 {
	public static void main(String[] args){
   BankAccount b1=new BankAccount("Pat",90);
   BankAccount b2=new BankAccount("Mike", 150);
   b1.deposit(80);
   b1.withdraw(59);
   System.out.println(b1.getOwner()+ ": "+ b1.getBalance());
  

//Compare objects with equal
	System.out.println(b1.equals(b2));
	System.out.println(b1.equals(b1));
	
	System.out.println("HashCode for first: "+b1.hashCode()+"\nHashCode for second: "+b2.hashCode());
	}
	

}

