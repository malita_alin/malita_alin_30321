package Lab6;

import java.util.*;



class Bank{
	private List<BankAccount> accounts = new ArrayList<BankAccount>();
	public void  addAccount(String owner, double balance) {accounts.add(new BankAccount(owner, balance));}
	
	public void printAccounts() { 
		SortbyBalance();
		for(int i=0; i<accounts.size(); i++)
		{System.out.println( ((BankAccount) accounts.get(i)).getOwner()+" "+((BankAccount) accounts.get(i)).getBalance() );}
	}
	
	public void SortbyBalance() {
		BankAccount temp;
		for(int i=0; i<accounts.size(); i++)
		{
			for(int j=0; j<accounts.size(); j++)
			{
				if( ((BankAccount) accounts.get(i)).getBalance() < ((BankAccount) accounts.get(j)).getBalance() ) {
                    temp = (BankAccount) accounts.get(i);
                    accounts.set(i, (BankAccount) accounts.get(j));
                    accounts.set(j, temp); }
				} } }
	
	public void SortbyName() {
		BankAccount temp;
		for(int i=0; i<accounts.size(); i++)
		{
			for(int j=0; j<accounts.size(); j++)
			{
				if( ((BankAccount) accounts.get(j)).getOwner().compareTo(((BankAccount) accounts.get(i)).getOwner()) > 0) {
                    temp = (BankAccount) accounts.get(i);
                    accounts.set(i, (BankAccount) accounts.get(j));
                    accounts.set(j, temp); }
				} } }
	
	
	public void printAccounts(double minBalance, double maxBalance) { SortbyBalance();
	for(int i=0; i<accounts.size(); i++)
	{if(((BankAccount) accounts.get(i)).getBalance() >= minBalance && ((BankAccount) accounts.get(i)).getBalance() <= maxBalance )
			{System.out.println( ((BankAccount) accounts.get(i)).getOwner()+" "+((BankAccount) accounts.get(i)).getBalance() );} 
	} }
	
	public void printAccount(BankAccount bankaccount)
	{System.out.println( bankaccount.getOwner()+" "+bankaccount.getBalance() );}
	
	
	public BankAccount getAccount(String Owner) {
	for(int i=0; i<accounts.size(); i++)
		{
		if( ((BankAccount) accounts.get(i)).getOwner()== Owner) return (BankAccount) accounts.get(i); }
	return null;
	}
	
	public  List<BankAccount> getAllAccounts() {
		SortbyName();
		return accounts;
	}
}


public class Exercise2 {
	public static void main(String[] args){
		Bank btr = new Bank();
		btr.addAccount("Doru Popa",192);
		btr.addAccount("Daniel Pop",292);
		btr.addAccount("Petru Baba",109);
		btr.addAccount("Alex Hirsh",150);
		btr.printAccounts();
		
		btr.printAccount(btr.getAccount("Petru Baba"));
		btr.getAccount("Petru Baba").deposit(11);
	    btr.printAccounts(120,190);
        System.out.println(btr.getAllAccounts());
	    
	}
}
