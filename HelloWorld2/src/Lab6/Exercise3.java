package Lab6;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;


class Bank1{
	private TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();
	public void  addAccount(String owner, double balance) {accounts.add(new BankAccount(owner, balance));}
	
	public void printAccounts() { 
		//SortbyBalance();
		System.out.println(accounts);
	}
	
	/*public void SortbyBalance() {
		Iterator <BankAccount> i=accounts.iterator();
		Iterator <BankAccount> j=accounts.iterator();
		while(i.hasNext()) {
			while(j.hasNext()) {
			if(i.next().getBalance()<j.next().getBalance())
			{System.out.println(i.next());}
			}
		}	
	} // TreeSet + BankAccount.compareTo automatically sorts in ascending order the Bank accounts, considering the Balance*/

	
	public void printAccounts(double minBalance,double maxBalance) {
        Iterator<BankAccount> i=accounts.iterator();
        while(i.hasNext()) {
            BankAccount s=i.next();
            if(s.getBalance()>=minBalance && s.getBalance()<=maxBalance)
                System.out.println(s);
        }
    }
	public void printAccount(BankAccount bankaccount)
	{System.out.println( bankaccount.getOwner()+" "+bankaccount.getBalance() );}
	
	
	public BankAccount getAccount(String Owner) {
		Iterator <BankAccount> i=accounts.iterator();
		
		while(i.hasNext()) {
			BankAccount s=i.next();
			if(Owner==s.getOwner())
				return s;
		}
		return null;
	}
	
	public TreeSet<BankAccount> getAllAccounts() {
		 TreeSet<BankAccount> orderedaccounts= new TreeSet<BankAccount>(new OwnerComparator());
		 orderedaccounts.addAll(accounts);
		return orderedaccounts;
	}
}


class OwnerComparator implements Comparator<BankAccount>
{
    public int compare(BankAccount b1, BankAccount b2)
    {
        return b1.getOwner().compareTo(b2.getOwner());
    }
}



public class Exercise3 {
	
	public static void main(String[] args){
		Bank1 bcr = new Bank1();
	    bcr.addAccount("Doru Popa",192);
		bcr.addAccount("Daniel Pop",292);
		bcr.addAccount("Petru Baba",109);
		bcr.addAccount("Alex Hirsh",150);
		
	  	bcr.printAccounts();
		
		bcr.printAccount(bcr.getAccount("Petru Baba"));
		bcr.getAccount("Petru Baba").deposit(11);
	    bcr.printAccounts(120,190);
        System.out.println(bcr.getAllAccounts()); 
	}
}
