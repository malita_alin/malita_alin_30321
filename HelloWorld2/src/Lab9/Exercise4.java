package Lab9;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

// X&0 game

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Exercise4 extends JFrame{
	
	JLabel textfield = new JLabel();
	JButton [] button  = new JButton [9]; //array of buttons
	  int count = 0;
    
     Exercise4()
     {
   	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   	   setLayout(new GridLayout(3, 3));
   	  init();
   	 setSize(615,740);
     setTitle("Tic Tac Toe");
     setVisible(true);
     
     }

     public void init(){
    	 this.setLayout(null);
         int width=200;int height = 200;
         
         textfield.setForeground(new Color(95, 235, 154));
         textfield.setHorizontalAlignment(JLabel.CENTER);
         textfield.setText("Tic-Tac-Toe");
         textfield.setFont(new Font("Arial",Font.BOLD, 28));
         textfield.setBounds(0,0,width*3, height/2);
         textfield.setOpaque(true);
        add(textfield);
         
         
         int bx=0, by=100; //button coordinates
         count = 0;
         for (int i = 0; i < 9; ++i) {
             button [i] = new JButton ("");
             bx=0; by=100; //button coordinates
             if(i==1||i==4||i==7) {bx=200;}
             if(i==2||i==5||i==8) {bx=400;}
             if(i==3||i==4||i==5) {by=300;}
             if(i==6||i==7||i==8) {by=500;}
             button [i].setBounds(bx,by,width, height);
             button [i].addActionListener(new TicTacToe());
             add (button [i]);
         }
         setVisible (true);
     }

     class TicTacToe implements ActionListener{
   	  @Override
   	  public void actionPerformed(ActionEvent e) {   
         String letter = (++count % 2 == 1) ? "X" : "O";
         /*Display X's or O's on the buttons*/
         for (JButton jb : button) 
         if (e.getSource () == jb) {
             jb.setText (letter);
            jb.setFont(new Font("Arial",Font.BOLD, 28));
            
            if(count % 2 == 1) { 
            	  textfield.setText("O turn");  //shows who's turn it is
            	  jb.setBackground(Color.BLACK); //set the color of button pressed
          }
            else {textfield.setText("X turn");
            	jb.setBackground(Color.WHITE);
            } 
             jb.setEnabled (false);
            matchCheck();
         }
        
     }
}
	
     
    public void matchCheck() {
         if ((button[0].getText() == "X") && (button[1].getText() == "X") && (button[2].getText() == "X")) {
             xWins(0, 1, 2);}
         else if ((button[0].getText() == "X") && (button[4].getText() == "X") && (button[8].getText() == "X")) {
             xWins(0, 4, 8); }
         else if ((button[0].getText() == "X") && (button[3].getText() == "X") && (button[6].getText() == "X")) {
             xWins(0, 3, 6);}
         else if ((button[1].getText() == "X") && (button[4].getText() == "X") && (button[7].getText() == "X")) {
             xWins(1, 4, 7); }
         else if ((button[2].getText() == "X") && (button[4].getText() == "X") && (button[6].getText() == "X")) {
             xWins(2, 4, 6);}
         else if ((button[2].getText() == "X") && (button[5].getText() == "X") && (button[8].getText() == "X")) {
             xWins(2, 5, 8); }
        else if ((button[3].getText() == "X") && (button[4].getText() == "X") && (button[5].getText() == "X")) {
             xWins(3, 4, 5); }
        else if ((button[6].getText() == "X") && (button[7].getText() == "X") && (button[8].getText() == "X")) {
             xWins(6, 7, 8);} //All X winning options
       
         else if ((button[0].getText() == "O") && (button[1].getText() == "O") && (button[2].getText() == "O")) {
             oWins(0, 1, 2); }
         else if ((button[0].getText() == "O") && (button[3].getText() == "O") && (button[6].getText() == "O")) {
             oWins(0, 3, 6);  }
         else if ((button[0].getText() == "O") && (button[4].getText() == "O") && (button[8].getText() == "O")) {
             oWins(0, 4, 8); }
         else if ((button[1].getText() == "O") && (button[4].getText() == "O") && (button[7].getText() == "O")) {
             oWins(1, 4, 7); }
         else if ((button[2].getText() == "O") && (button[4].getText() == "O") && (button[6].getText() == "O")) {
             oWins(2, 4, 6); }
         else if ((button[2].getText() == "O") && (button[5].getText() == "O") && (button[8].getText() == "O")) {
             oWins(2, 5, 8); }
         else if ((button[3].getText() == "O") && (button[4].getText() == "O") && (button[5].getText() == "O")) {
             oWins(3, 4, 5); } 
         else if ((button[6].getText() == "O") && (button[7].getText() == "O") && (button[8].getText() == "O")) {
             oWins(6, 7, 8); }   //All O winning options
         else if(count==9) {
             textfield.setText("Match Tie");
              gameOver("Match Tie");
         }
     }
     public void xWins(int x1, int x2, int x3) {
         button[x1].setBackground(Color.GREEN);
         button[x2].setBackground(Color.GREEN);
         button[x3].setBackground(Color.GREEN);
         for (int i = 0; i < 9; i++) {
             button[i].setEnabled(false);
         }
         textfield.setText("X Wins!");
         gameOver("X Wins!");
     }
     
     public void oWins(int x1, int x2, int x3) {
         button[x1].setBackground(Color.GREEN);
         button[x2].setBackground(Color.GREEN);
         button[x3].setBackground(Color.GREEN);
         for (int i = 0; i < 9; i++) {
             button[i].setEnabled(false);
         }
         textfield.setText("O Wins!");
         gameOver("O Wins!");
     }
     public void gameOver(String s){
         count=0;
         Object[] option={"Restart","Exit"};
         int n=JOptionPane.showOptionDialog(null, "Game Over\n"+s,"Game Over",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,option,option[0]);
         if(n==0){
             dispose();
             new Exercise4();
         }
         else{
             dispose();
         }
     }
     
     
	 public static void main(String args[])
     {
           Exercise4 a=new Exercise4();
     }
 
}