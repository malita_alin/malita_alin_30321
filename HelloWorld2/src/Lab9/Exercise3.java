package Lab9;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import Lab9.Exercise5.Controler;
import Lab9.Exercise5.Segment;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
public class Exercise3 extends JFrame
{
	 JLabel filename;
     JTextField tfilename;
     JTextArea tArea;
     JTextArea tAreafile;
     JButton bSearch;
 
      Exercise3()
      {
    	  setTitle("File Opener");
    	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	  init();
            setSize(800,300);
            setVisible(true);
      }
 
      public void init(){
    	  
          this.setLayout(null);
          int width=100;int height = 100;
          filename = new JLabel("FileName: ");
          filename.setBounds(30, 5, width, height/2);
          
          bSearch = new JButton("Search");
          bSearch.setBounds(50,85,width, height/3);

          bSearch.addActionListener(new SearchButton());
          tArea = new JTextArea();
          tArea.setBounds(90,20,width*5,height/4);
          
          
          tAreafile = new JTextArea();
          tAreafile.setEditable(false);  
          tAreafile.setLineWrap(true);
          tAreafile.setWrapStyleWord(true);
          tAreafile.setBounds(200,60,width*4,height*2);
          
          bSearch.addActionListener(new SearchButton());
          add(bSearch);
          add(filename); add(tArea); add(tAreafile);
      }
 
      
 
      public static void main(String args[])
      {
            Exercise3 a=new Exercise3();
      }
      
      class SearchButton implements ActionListener{
    	  @Override
    	  public void actionPerformed(ActionEvent e) {
    		  String filePath = Exercise3.this.tArea.getText();
    		  Path folder=Path.of(filePath);
  			try {
  				String output=Files.readString(folder);
  				Exercise3.this.tAreafile.setText(output);
  			} catch (IOException e1) {
  				 Exercise3.this.tAreafile.append("File not found\n");
  				e1.printStackTrace();
  			}
             
    	  
    	 }
}
}