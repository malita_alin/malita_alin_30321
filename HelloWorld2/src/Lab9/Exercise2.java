package Lab9;
// counter
import java.awt.*;        
import java.awt.event.*;  

import javax.swing.JFrame;


public class Exercise2  extends JFrame {
	   private Label lblCount;    // Declare a Label component
	   private TextField tfCount; // Declare a TextField component
	   private Button btnCount;   // Declare a Button component
	   private int count = 0;     // Counter's value


	   public Exercise2 () {
	      setLayout(new FlowLayout());
	 

	      lblCount = new Label("Counter");  // construct the Label component
	      add(lblCount);                    // "super" Frame container adds Label component

	      tfCount = new TextField(count + "", 10); // construct the TextField component with initial text
	      tfCount.setEditable(false);       // set to read-only
	      add(tfCount);                     // "super" Frame container adds TextField component

	      btnCount = new Button("Count");   // construct the Button component
	      add(btnCount);                    // "super" Frame container adds Button component

	      BtnCountListener listener = new BtnCountListener();
	      btnCount.addActionListener(listener);
	      setTitle(" Counter");  // "super" Frame sets its title
	      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      setSize(300, 100);        // "super" Frame sets its initial window size
	      setVisible(true);         
	   }

	  
	// The entry main() method
	   public static void main(String[] args) {

	      Exercise2 app = new Exercise2();
	   }

	   // Define an inner class to handle the "Count" button-click
	   private class BtnCountListener implements ActionListener {
	      // ActionEvent handler - Called back upon button-click.
	      @Override
	      public void actionPerformed(ActionEvent evt) {
	         ++count; // Increase the counter value
	         // Display the counter value on the TextField tfCount
	         tfCount.setText(count + ""); // Convert int to String
	      }
	   }
	}

