package Lab9;


import java.lang.ModuleLayer.Controller;
import java.util.*;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

// X&0 game

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Exercise5 extends JFrame{
	
	 JLabel segmentlabel;
	 JLabel trainlabel;
	 JLabel destinationlabel;
	 JLabel interfacelabel;
     JTextField tfilename;
     JTextArea tArea;
     JTextArea tTrainname;
     JTextArea tDestination;
     JTextArea tSegment;
     JButton bAdd;
     JButton bSimulator;
     JButton bExit;
     JScrollPane scroll;
     JPanel contentPane;
	/*
	 * @param args
	 */
     
   
     
	Exercise5()
    {
		 setTitle("Train Simulator");
   	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   	  init();
           setSize(800,700);
           setVisible(true);
           
    
    }

	   public void init(){
	    	  
	          this.setLayout(null);
	          int width=100;int height = 100;
	          
	          trainlabel = new JLabel("Train name: ");
	          trainlabel.setBounds(20, 5, width, height/2);
	          tTrainname = new JTextArea();
	          tTrainname.setBounds(110,20,width*3,height/4);
	          
	          segmentlabel = new JLabel("Start Segment: ");
	          segmentlabel.setBounds(20, 35, width, height/2);
	          tSegment = new JTextArea();
	          tSegment.setBounds(110,50,width*3,height/4);
	          
	          destinationlabel = new JLabel("Destination: ");
	          destinationlabel.setBounds(20, 65, width, height/2);
	          tDestination = new JTextArea();
	          tDestination.setBounds(110,80,width*3,height/4);
	          
	          bAdd = new JButton("Add Train");
	          bAdd.setBounds(70,115,width, height/3);

	          interfacelabel = new JLabel("Output: ");
	          interfacelabel.setBounds(50, 150, width, height/2);
	          interfacelabel.setFont(new Font("Arial",Font.BOLD, 24));
	           
	          contentPane= new JPanel();
	          contentPane.setBounds(30,190,width*6+10,height*5);
              add(contentPane);
	          tArea = new JTextArea();
	          tArea.setEditable(false);  
	          tArea.setLineWrap(true);
	          tArea.setWrapStyleWord(true);
	          tArea.setBounds(30,190,width*6,height*5);
	          contentPane.add(tArea,BorderLayout.CENTER);
	          JScrollPane scroll = new JScrollPane(tArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	          contentPane.add(scroll);
	          
	          bSimulator = new JButton("Simulate");
	          bSimulator.setBounds(570,45,width*3/2, height/3);
	          
	          bExit = new JButton("Restart/Quit");
	          bExit.setBounds(570,85,width*3/2, height/3);
	          
	          
	          bAdd.addActionListener(new AddTrainButton());
	          add(bAdd);
	          
	          bExit.addActionListener(new ExitButton());
	          add(bExit);
	          
	          bSimulator.addActionListener(new SimulatorButton());
	          add(bSimulator);
	          
	          add(trainlabel);  add(segmentlabel); add(destinationlabel); add(interfacelabel);
	           add(tTrainname); add(tDestination); add(tSegment);  
	          
	          //setup for controlers 
	          for(int i=0; i<9;i++)
	          {s[i]=new Segment(i+1);
	          }
	          c1.addControlledSegment(s[0]);
	     		c1.addControlledSegment(s[1]);
	     		c1.addControlledSegment(s[2]);

	     		c2.addControlledSegment(s[3]);
	     		c2.addControlledSegment(s[4]);
	     		c2.addControlledSegment(s[5]);
	     		
	     		c3.addControlledSegment(s[6]);
	     		c3.addControlledSegment(s[7]);
	     		c3.addControlledSegment(s[8]);
	     	//neighbour relations
	     	c1.setNeighbourControllers(c2);
	     	c1.setNeighbourControllers(c3);
	     				
	     	c2.setNeighbourControllers(c1);
	     	c2.setNeighbourControllers(c3);
	     				
	     	c3.setNeighbourControllers(c1);
	     	c3.setNeighbourControllers(c2);
	     		
	     	s[0].arriveTrain(t1);
	     	s[4].arriveTrain(t2); 
	     	s[3].arriveTrain(t4); 
			s[2].arriveTrain(t3); 
	          displayAllStations(c1, c2, c3);
	      }
	 
	      
	 
	      public static void main(String args[])
	      {
	            Exercise5 a=new Exercise5();
	      }
    
	      ///
	      
	      Controler c1 = new Controler("Cluj-Napoca");
	      Segment s[]= new Segment[20]; 
	      Train[] t = new Train[10];
 		//build station Bucuresti
 		Controler c2 = new Controler("Bucuresti");
 		//build station Timisoara -
 		Controler c3 = new Controler("Timisoara");
 		
 		 int tcount=0;
			Train t1 = new Train("Bucuresti", "IC-001");
			Train t2 = new Train("Cluj-Napoca","R-002");
			Train t3= new Train("Timisoara","IR-003");
			Train t4= new Train("Bucuresti","IR-004");
 		///
 		
class AddTrainButton implements ActionListener{
	  @Override
	  public void actionPerformed(ActionEvent e) {
		  
		 
		   if(Exercise5.this.tSegment.getText().trim().length() != 0 && Exercise5.this.tDestination.getText().trim().length() != 0
				   && Exercise5.this.tTrainname.getText().trim().length() != 0) {
			   int x=Integer.parseInt(Exercise5.this.tSegment.getText());
            t[tcount]=new Train(Exercise5.this.tDestination.getText(),Exercise5.this.tTrainname.getText());
		  //  case tSegment-> add to certain segment
 		   s[x-1].arriveTrain(t[tcount]);
		   displayAllStations(c1, c2, c3);
		   tcount++;}
		   else JOptionPane.showMessageDialog(null, "Please Complete all Train add information");

		}

	 }

class ExitButton implements ActionListener{
	  @Override
	  public void actionPerformed(ActionEvent e) {
		  Object[] option={"Restart","Quit"};
	         int n=JOptionPane.showOptionDialog(null, "Restart or Quit Simulator\n","Restart/Exit",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,option,option[0]);
	         if(n==0){
	             dispose();
	             new Exercise5();
	         }
	         else{
	             dispose();
	         }
		}
	 }


class SimulatorButton implements ActionListener{
	  @Override
	  public void actionPerformed(ActionEvent e) {
		//controller init
     		
		   tArea.append("\nStart train control\n");
 
                //execute 3 times controller steps
		for(int i = 0;i<3;i++){
			tArea.append("\n### Step "+i+" ###");
			c1.controlStep();
			c2.controlStep();
			c3.controlStep();
			displayAllStations(c1, c2, c3);
		}
	 } }

     public void displayAllStations(Controler c1, Controler c2, Controler c3)
     { tArea.append("\nCurrent Stations State");
    	 c1.displayStationState();
		c2.displayStationState();
		c3.displayStationState(); }
     

 
class Controler{
 
	String stationName;
	ArrayList<Segment> list = new ArrayList<Segment>(); 
	ArrayList<Controler> neighbourControllers = new ArrayList<Controler>(); //new neighbourControllers list
	
	public Controler(String gara) {
		stationName = gara;
	}
 
   void setNeighbourControllers(Controler c) { //new method
   neighbourControllers.add(c);
}
	void addControlledSegment(Segment s){
		list.add(s);
	}
 
	/**
         * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
         * 
	 * @return
	 */
	int getFreeSegmentId(){
		for(Segment s:list){
			if(s.hasTrain()==false)
				return s.id;
		}
		return -1;
	}
 
	void controlStep(){
			//check which train must be sent
		
			for(Segment segment:list){
				if(segment.hasTrain()){
					Train t = segment.getTrain();
					int i=0; 
					for(Controler neighboursControllers:neighbourControllers) {
						Controler neighbours =neighbourControllers.get(i);
						i++;
					if(t.getDestination().equals(neighbours.stationName)){
						//check if there is a free segment
						int id = neighbours.getFreeSegmentId();
						if(id==-1){
							tArea.append("\nTrenul "+t.name+" din gara "+stationName+" nu poate fi trimis catre "+neighbours.stationName+". Nici un segment disponibil!");
							return;
						}
						//send train
						tArea.append("\nTrenul "+t.name+" pleaca din gara "+stationName +" spre gara "+neighbours.stationName);
						segment.departTRain();
						neighbours.arriveTrain(t,id);
					}
					}	
				}
					
					
			}//.for
 
		}  //.
 
 
	public void arriveTrain(Train t, int idSegment){
		for(Segment segment:list){
			//search id segment and add train on it
			if(segment.id == idSegment)
				if(segment.hasTrain()==true){
					tArea.append("\n CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
					return;
				}else{
					tArea.append("\n Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
					segment.arriveTrain(t);
					return;
				}			
		}
 
		//this should not happen
		tArea.append("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");
 
	}
 
 
	public void displayStationState(){
		tArea.append("\n=== STATION "+stationName+" ===");
		for(Segment s:list){
			if(s.hasTrain())
				tArea.append("\n|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
			else
				tArea.append("\n|----------ID="+s.id+"__Train=______ catre ________----------|");
		}
	}
}
 
 
class Segment{
	int id;
	Train train;
 
	Segment(int id){
		this.id = id;
	}
 
	boolean hasTrain(){
		return train!=null;
	}
 
	Train departTRain(){
		Train tmp = train;
		this.train = null;
		return tmp;
	}
 
	void arriveTrain(Train t){
		train = t;
	}
 
	Train getTrain(){
		return train;
	}
}
 
class Train{
	String destination;
	String name;
 
	public Train(String destinatie, String nume) {
		super();
		this.destination = destinatie;
		this.name = nume;
	}
   
	public void setName(String name)
	{ this.name=name;}
	public void setDestination(String destination)
	{ this.destination=destination;}
	
	String getDestination(){
		return destination;
	}
 
}

}